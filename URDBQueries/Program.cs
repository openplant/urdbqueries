﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenPlant;
using System.Data;
using System.IO;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace URDBQueries
{
    class Program
    {
        public static Stopwatch sw = new Stopwatch();
        public static string QueryCreateTableTagDatabaseLocation = "";


        static void Main(string[] args)
        {
            sw.Start();
            Global.Products.Add("URDBQueries", new ProductDetails()
            {
                ProductName = "URDBQueries",
                Config = new ConfigBase()
                {
                    LogFilePath = Global.CommonAppDataDir + @"\URDBQueries\URDBQueries.log",
                    EnableLogToServer = true,
                    EnableLogToLocal = false,
                    LogServerHost = "127.0.0.1",
                    LogLocalPort = 33171
                }
            });


            #region INITIATE DATABASE
            string ServerIP = "localhost";
            string MySqlUN = "root";
            string MySqlPW = "";
            string DatbaseName = "openplantserver";
            clsMySqlConnector MySqlConn = new clsMySqlConnector("Server=" + ServerIP + ";Port=3306;Database=" + DatbaseName + ";Uid=" + MySqlUN + ";Pwd=" + MySqlPW + ";");

            //Read address text file and populate it
            //Read all the tags from the text file {tags.txt} and store in Parameters
            IEnumerable<String> FileReadOutput = File.ReadLines("tags.txt");

            // Shows a List of KeyValuePairs.
            var TagItems = new List<TagItem>();
            foreach (String Line in FileReadOutput)
            {
                string[] LineSplit = Line.Split(' ');
                string LineSplitAddress = LineSplit[0].Trim();
                string LineSplitParameter = LineSplit[1].Trim();
                string LineSplitDatatype = LineSplit[2].Trim();
                string LineSplitRange = LineSplit[3].Trim();
                DataType LineSplitTagDataType = new DataType();


                //public enum DataType { Boolean, Angle, Velocity, Current, Temperature, Mode, RevolutionCount, State }
                switch (LineSplitDatatype.ToLower())
                {
                    case "boolean":
                        LineSplitTagDataType = DataType.Boolean;
                        break;
                    case "angle":
                        LineSplitTagDataType = DataType.Angle;
                        break;
                    case "velocity":
                        LineSplitTagDataType = DataType.Velocity;
                        break;
                    case "current":
                        LineSplitTagDataType = DataType.Current;
                        break;
                    case "temperature":
                        LineSplitTagDataType = DataType.Temperature;
                        break;
                    case "mode":
                        LineSplitTagDataType = DataType.Mode;
                        break;
                    case "revolutioncount":
                        LineSplitTagDataType = DataType.RevolutionCount;
                        break;
                    case "state":
                        LineSplitTagDataType = DataType.State;
                        break;
                    default:
                        break;

                }

                TagItems.Add(new TagItem() { Address = LineSplitAddress, Parameter = LineSplitParameter, TagDataType = LineSplitTagDataType, Range = LineSplitRange });
            }
            #endregion

            CreateAllTables(MySqlConn, TagItems);

            CreateTableTags(MySqlConn, TagItems);

            PopulateTags(MySqlConn, TagItems);

            PopulateDaarchiveconfig(MySqlConn, TagItems);

            PopulateTagDatabaseLocation(MySqlConn, TagItems);
        }

        private static void CreateTableTags(clsMySqlConnector mySqlConn, List<TagItem> TagItems)
        {
            //
            //Create table `tags`
            string CreateTableTags = String.Format(@"
                CREATE TABLE `tags` (
                  `TagName` varchar(255) NOT NULL,
                  `Parameter` varchar(255) NOT NULL,
                  `DatabaseLocation` varchar(255) NOT NULL,
                  `UnitOfMeasure` varchar(255) DEFAULT NULL,
                  `Description` varchar(1000) DEFAULT NULL,
                  `Corporate` varchar(255) DEFAULT NULL,
                  `Plant` varchar(255) DEFAULT NULL,
                  `Area` varchar(255) DEFAULT NULL,
                  `Unit` varchar(255) DEFAULT NULL,
                  `DataType` tinyint(4) NOT NULL DEFAULT '0',
                  `CacheSizeInNumberOfItems` int(11) NOT NULL DEFAULT '3600',
                  `DuplicateThresholdInMS` int(11) NOT NULL DEFAULT '500',
                  `ChartInterpolationType` tinyint(4) NOT NULL DEFAULT '1',
                  `MinRange` double DEFAULT NULL,
                  `MaxRange` double DEFAULT NULL,
                  `P1` varchar(255) DEFAULT NULL,
                  `P2` varchar(255) DEFAULT NULL,
                  `P3` varchar(255) DEFAULT NULL,
                  `P4` varchar(255) DEFAULT NULL,
                  `P5` varchar(255) DEFAULT NULL,
                  `P6` varchar(255) DEFAULT NULL,
                  `P7` varchar(255) DEFAULT NULL,
                  `P8` varchar(255) DEFAULT NULL,
                  `P9` varchar(255) DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=latin1"
            );
            //File.WriteAllText(@"C:\Users\Sifat\Desktop\CreateTableTags.sql", CreateTableTags);
            mySqlConn.Query(CreateTableTags);
        }

        private static void CreateAllTables(clsMySqlConnector mySqlConn, List<TagItem> TagItems)
        {
            #region Table Structure
            /*
            --
            --Table structure for table `daarchiveconfig`
            --

           CREATE TABLE `daarchiveconfig` (
             `DSConnName` varchar(255) NOT NULL,
             `DAItemId` varchar(255) NOT NULL,
             `TagId` varchar(255) NOT NULL
           ) ENGINE = InnoDB DEFAULT CHARSET = latin1;

            --
            -- Table structure for table `tags`
            --

            CREATE TABLE `tags` (
              `TagName` varchar(255) NOT NULL,
              `Parameter` varchar(255) NOT NULL,
              `DatabaseLocation` varchar(255) NOT NULL,
              `UnitOfMeasure` varchar(255) DEFAULT NULL,
              `Description` varchar(1000) DEFAULT NULL,
              `Corporate` varchar(255) DEFAULT NULL,
              `Plant` varchar(255) DEFAULT NULL,
              `Area` varchar(255) DEFAULT NULL,
              `Unit` varchar(255) DEFAULT NULL,
              `DataType` tinyint(4) NOT NULL DEFAULT '0',
              `CacheSizeInNumberOfItems` int(11) NOT NULL DEFAULT '3600',
              `DuplicateThresholdInMS` int(11) NOT NULL DEFAULT '500',
              `ChartInterpolationType` tinyint(4) NOT NULL DEFAULT '1',
              `MinRange` double DEFAULT NULL,
              `MaxRange` double DEFAULT NULL,
              `P1` varchar(255) DEFAULT NULL,
              `P2` varchar(255) DEFAULT NULL,
              `P3` varchar(255) DEFAULT NULL,
              `P4` varchar(255) DEFAULT NULL,
              `P5` varchar(255) DEFAULT NULL,
              `P6` varchar(255) DEFAULT NULL,
              `P7` varchar(255) DEFAULT NULL,
              `P8` varchar(255) DEFAULT NULL,
              `P9` varchar(255) DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

            --
            -- Table structure for table `[da]random.int2`
            --

            CREATE TABLE `[da]random.int2` (
              `TSUTC` datetime NOT NULL,
              `Val` int(11) NOT NULL DEFAULT '0',
              `AggMark` tinyint(1) NOT NULL DEFAULT '0'

             */
            #endregion

            //Drop Database OpenPlantServer and Create New One
            string DropDatabaseCreateNewDatabase = String.Format(@"
                DROP DATABASE openplantserver;
                create database openplantserver;
                use openplantserver;"
            );
            mySqlConn.Query(DropDatabaseCreateNewDatabase);

            //Create list of 1 robots and call it TagNames
            int NumberOfRobots = 1;
            ObservableCollection<string> TagNames = new ObservableCollection<string>();
            for (int i = 1; i < NumberOfRobots + 1; i++)
            {
                TagNames.Add("RB0" + i);
            }

            //Create table `daarchiveconfig`
            string CreateTableDaarchiveconfig = String.Format(@"
               CREATE TABLE `daarchiveconfig` (
                 `DSConnName` varchar(255) NOT NULL,
                 `DAItemId` varchar(255) NOT NULL,
                 `TagId` varchar(255) NOT NULL
               ) ENGINE = InnoDB DEFAULT CHARSET = latin1"
            );
            mySqlConn.Query(CreateTableDaarchiveconfig);


            //Create table `{tags}+{parameters}`
            foreach (string TagName in TagNames)
            {
                string QueryCreateTableTagDatabaseLocation = "";
                //Loop through the {Paremeters}
                foreach (TagItem TagItem in TagItems)
                {
                    //TableName
                    string TableName = "[DA]" + TagName + "." + TagItem.Parameter;

                    switch (TagItem.TagDataType)
                    {
                        case DataType.Angle:
                            //Query String
                            QueryCreateTableTagDatabaseLocation = String.Format(@"          
                            CREATE TABLE `{0}` (
                                `TSUTC` datetime NOT NULL,
                                `Val` double NOT NULL DEFAULT '0',
                                `AggMark` tinyint(1) NOT NULL DEFAULT '0')", TableName);
                            break;
                        case DataType.Boolean:
                            //Query String
                            QueryCreateTableTagDatabaseLocation = String.Format(@"          
                            CREATE TABLE `{0}` (
                                `TSUTC` datetime NOT NULL,
                                `Val` tinyint(1) NOT NULL DEFAULT '0',
                                `AggMark` tinyint(1) NOT NULL DEFAULT '0')", TableName);
                            break;
                        case DataType.Current:
                            //Query String
                            QueryCreateTableTagDatabaseLocation = String.Format(@"          
                            CREATE TABLE `{0}` (
                                `TSUTC` datetime NOT NULL,
                                `Val` double NOT NULL DEFAULT '0',
                                `AggMark` tinyint(1) NOT NULL DEFAULT '0')", TableName);
                            break;
                        case DataType.Mode:
                            //Query String
                            QueryCreateTableTagDatabaseLocation = String.Format(@"          
                            CREATE TABLE `{0}` (
                                `TSUTC` datetime NOT NULL,
                                `Val` int(11) NOT NULL DEFAULT '0',
                                `AggMark` tinyint(1) NOT NULL DEFAULT '0')", TableName);
                            break;
                        case DataType.RevolutionCount:
                            //Query String
                            QueryCreateTableTagDatabaseLocation = String.Format(@"          
                            CREATE TABLE `{0}` (
                                `TSUTC` datetime NOT NULL,
                                `Val` int(11) NOT NULL DEFAULT '0',
                                `AggMark` tinyint(1) NOT NULL DEFAULT '0')", TableName);
                            break;
                        case DataType.State:
                            //Query String
                            QueryCreateTableTagDatabaseLocation = String.Format(@"          
                            CREATE TABLE `{0}` (
                                `TSUTC` datetime NOT NULL,
                                `Val` int(11) NOT NULL DEFAULT '0',
                                `AggMark` tinyint(1) NOT NULL DEFAULT '0')", TableName);
                            break;
                        case DataType.Temperature:
                            //Query String
                            QueryCreateTableTagDatabaseLocation = String.Format(@"          
                            CREATE TABLE `{0}` (
                                `TSUTC` datetime NOT NULL,
                                `Val` double NOT NULL DEFAULT '0',
                                `AggMark` tinyint(1) NOT NULL DEFAULT '0')", TableName);
                            break;
                        case DataType.Velocity:
                            //Query String
                            QueryCreateTableTagDatabaseLocation = String.Format(@"          
                            CREATE TABLE `{0}` (
                                `TSUTC` datetime NOT NULL,
                                `Val` double NOT NULL DEFAULT '0',
                                `AggMark` tinyint(1) NOT NULL DEFAULT '0')", TableName);
                            break;
                    }

                    //Query
                    mySqlConn.Query(QueryCreateTableTagDatabaseLocation);
                }
            }

        }

        private static void PopulateTags(clsMySqlConnector MySqlConn, List<TagItem> TagItems)
        {

            //Clear previous data
            MySqlConn.Query("DELETE FROM tags");

            //Create list of 1 robots and call it TagNames
            int NumberOfRobots = 1;
            ObservableCollection<string> TagNames = new ObservableCollection<string>();
            for (int i = 1; i < NumberOfRobots + 1; i++)
            {
                TagNames.Add("RB00" + i);
            }
            string QueryInsertATag="";

            //INSERT TAG RECORDS INTO TAGS
            //Loop through the {TagNames}
            foreach (string TagName in TagNames)
            {
                //Loop through the {Paremeters}
                foreach (TagItem TagItem in TagItems)
                {
                    string UnitOfMeasure="";
                    int TagItemDataType = 0;
                    switch (TagItem.TagDataType)
                    {
                        //0 - real
                        //1 - interger
                        //2 - bool
                        case DataType.Angle:
                            UnitOfMeasure = "degree";
                            TagItemDataType = 0;
                            break;
                        case DataType.Boolean:
                            UnitOfMeasure = null;
                            TagItemDataType = 2;
                            break;
                        case DataType.Current:
                            UnitOfMeasure = "mA";
                            TagItemDataType = 0;
                            break;
                        case DataType.Mode:
                            UnitOfMeasure = null;
                            TagItemDataType = 1;
                            break;
                        case DataType.RevolutionCount:
                            UnitOfMeasure = null;
                            TagItemDataType =1;
                            break;
                        case DataType.State:
                            UnitOfMeasure = null;
                            TagItemDataType = 1;
                            break;
                        case DataType.Temperature:
                            UnitOfMeasure = "celsius";
                            TagItemDataType = 0;
                            break;
                        case DataType.Velocity:
                            UnitOfMeasure = "degree/seconds";
                            TagItemDataType = 0;
                            break;
                    }

                    //Insert a Record
                    QueryInsertATag += String.Format(@"insert into tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`,`datatype`) values ('{0}','{1}','{2}','{3}','{4}');", TagName, TagItem.Parameter, UnitOfMeasure, "[DA]" + TagName + "." + TagItem.Parameter, TagItemDataType);

                }
            }
            File.WriteAllText(@"C:\Users\sifatsultan\Desktop\QueryInsertATag.sql", QueryInsertATag);
            MySqlConn.Query(QueryInsertATag);
        }

        private static void PopulateTagDatabaseLocation(clsMySqlConnector mySqlConn, List<TagItem> TagItems)
        {

            //Create list of 1 robots and call it TagNames
            int NumberOfRobots = 1;
            ObservableCollection<string> TagNames = new ObservableCollection<string>();
            for (int i = 1; i < NumberOfRobots + 1; i++)
            {
                TagNames.Add("RB0" + i);
            }

            //Generate between 0 to -1
            var randomDouble = new Random(DateTime.Now.Millisecond);
            var randomInt = new Random(DateTime.Now.Millisecond);
            var randomBoolean = new Random(DateTime.Now.Millisecond);

            //10 days from today
            //everyday take the day and loop through 24*60*60 seconds
            //values are made using RandomWalk
            int NumberOfDays = 7;
            //Loop through 24*60*60=86,400 seconds for each single day
            int NumberOfReadingsPerDay = (24 * 60);
            DateTime[] PastNumberOfDays = Enumerable.Range(0, NumberOfDays)
                .Select(i => DateTime.Now.Date.AddDays(-i))
                .ToArray();

            string PopulateTagDatabase = "";
            //Loop through all robot
            int count = 0;
            foreach (string TagName in TagNames)
            {
                
                //For each single parameter in a robot
                foreach (TagItem TagItem in TagItems)
                {
                    count++;
                    
                    //Sample TableName = [DA]RB001.arm1AngleVelocity
                    string TableName = "[DA]" + TagName + "." + TagItem.Parameter;
                    PopulateTagDatabase += "insert into `" + TableName + "` (`tsutc`,`val`,`aggmark`) values ";
                    //TagItem.TagDataType = DataType.Temperature;
                    switch (TagItem.TagDataType)
                    {
                        case DataType.Angle:
                            //Loop through PastNumberOfDays day for each single parameter
                            foreach (var Day in PastNumberOfDays)
                            {
                                var Random = new Random();
                                for (int j = 0; j < NumberOfReadingsPerDay; j++)
                                {
                                    double Val = Math.Round(Random.NextDouble() * 360, 1);
                                    //Val, 1);
                                    //Query String
                                    PopulateTagDatabase += "( '" + Day.AddMinutes(j).ToString("yyyy-MM-dd HH:mm:ss") + "' , '" + Val + "' , 0 ),";
                                }
                            }
                            PopulateTagDatabase = PopulateTagDatabase.Substring(0, (PopulateTagDatabase.Length - 1));
                            PopulateTagDatabase += ";";
                            break;
                        case DataType.Boolean:
                            //Loop through PastNumberOfDays day for each single parameter
                            foreach (var Day in PastNumberOfDays)
                            {
                                var Random = new Random(DateTime.Now.Millisecond);
                                //Loop through 24*60*60=86,400 seconds for each single day
                                for (int j = 0; j < NumberOfReadingsPerDay; j++)
                                {
                                    // the value needs to vary between 0 and 1
                                    int Val = Random.Next(2);
                                    //Query String
                                    PopulateTagDatabase += "( '" + Day.AddMinutes(j).ToString("yyyy-MM-dd HH:mm:ss") + "' , '" + Val + "' , 0 ),";
                                }
                            }
                            PopulateTagDatabase = PopulateTagDatabase.Substring(0, (PopulateTagDatabase.Length - 1));
                            PopulateTagDatabase += ";";
                            break;
                        case DataType.Current:
                            //Loop through PastNumberOfDays day for each single parameter
                            foreach (var Day in PastNumberOfDays)
                            {
                                var Random = new Random();
                                //Loop through 24*60*60=86,400 seconds for each single day
                                for (int j = 0; j < NumberOfReadingsPerDay; j++)
                                {
                                    //lets imagine the range is 0-20
                                    double Val = Math.Round((Random.NextDouble() * 20), 1);
                                    //Query String
                                    PopulateTagDatabase += "( '" + Day.AddMinutes(j).ToString("yyyy-MM-dd HH:mm:ss") + "' , '" + Val + "' , 0 ),";
                                }
                            }
                            PopulateTagDatabase = PopulateTagDatabase.Substring(0, (PopulateTagDatabase.Length - 1));
                            PopulateTagDatabase += ";";
                            break;
                        case DataType.Mode:
                            //Loop through PastNumberOfDays day for each single parameter
                            foreach (var Day in PastNumberOfDays)
                            {
                                var Random = new Random();
                                //Loop through 24*60*60=86,400 seconds for each single day
                                for (int j = 0; j < NumberOfReadingsPerDay; j++)
                                {
                                    //the value needs to vary betweeen 0 and 10
                                    double Val = Random.Next(10);
                                    //Query String
                                    PopulateTagDatabase += "( '" + Day.AddMinutes(j).ToString("yyyy-MM-dd HH:mm:ss") + "' , '" + Val + "' , 0 ),";
                                }
                            }
                            PopulateTagDatabase = PopulateTagDatabase.Substring(0, (PopulateTagDatabase.Length - 1));
                            PopulateTagDatabase += ";";
                            break;
                        case DataType.RevolutionCount:
                            //Loop through PastNumberOfDays day for each single parameter
                            foreach (var Day in PastNumberOfDays)
                            {
                                var Random = new Random();
                                //Loop through 24*60*60=86,400 seconds for each single day
                                for (int j = 0; j < NumberOfReadingsPerDay; j++)
                                {
                                    //the value needs to vary between 0 and 30
                                    double Val = Random.Next(30);
                                    //Query String
                                    PopulateTagDatabase += "( '" + Day.AddMinutes(j).ToString("yyyy-MM-dd HH:mm:ss") + "' , '" + Val + "' , 0 ),";
                                }
                            }
                            PopulateTagDatabase = PopulateTagDatabase.Substring(0, (PopulateTagDatabase.Length - 1));
                            PopulateTagDatabase += ";";
                            break;
                        case DataType.State:
                            //Loop through PastNumberOfDays day for each single parameter
                            foreach (var Day in PastNumberOfDays)
                            {
                                var Random = new Random();
                                //Loop through 24*60*60=86,400 seconds for each single day
                                for (int j = 0; j < NumberOfReadingsPerDay; j++)
                                {
                                    //lets say there are 4 states
                                    double Val = Random.Next(4);
                                    //Query String
                                    PopulateTagDatabase += "( '" + Day.AddMinutes(j).ToString("yyyy-MM-dd HH:mm:ss") + "' , '" + Val + "' , 0 ),";
                                }
                            }
                            PopulateTagDatabase = PopulateTagDatabase.Substring(0, (PopulateTagDatabase.Length - 1));
                            PopulateTagDatabase += ";";
                            break;
                        case DataType.Temperature:
                            //Loop through PastNumberOfDays day for each single parameter
                            foreach (var Day in PastNumberOfDays)
                            {
                                var Random = new Random(DateTime.Now.Minute);
                                //Loop through 24*60*60=86,400 seconds for each single day
                                for (int j = 0; j < NumberOfReadingsPerDay; j++)
                                {
                                    //temperature will vary between 0.0 and 40.0
                                    double Val = Math.Round(Random.NextDouble() * 40, 1);
                                    //Query String
                                    PopulateTagDatabase += "( '" + Day.AddMinutes(j).ToString("yyyy-MM-dd HH:mm:ss") + "' , '" + Val + "' , 0 ),";
                                }
                            }
                            PopulateTagDatabase = PopulateTagDatabase.Substring(0, (PopulateTagDatabase.Length - 1));
                            PopulateTagDatabase += ";";
                            break;
                        case DataType.Velocity:
                            //Loop through PastNumberOfDays day for each single parameter
                            foreach (var Day in PastNumberOfDays)
                            {
                                var Random = new Random();
                                //Loop through 24*60*60=86,400 seconds for each single day
                                for (int j = 0; j < NumberOfReadingsPerDay; j++)
                                {
                                    //velocity will vary between 0.0 and 10.0
                                    double Val = Math.Round((Random.NextDouble() * 10), 1);
                                    //Query String
                                    PopulateTagDatabase += "( '" + Day.AddMinutes(j).ToString("yyyy-MM-dd HH:mm:ss") + "' , '" + Val + "' , 0 ),";
                                }
                            }
                            PopulateTagDatabase = PopulateTagDatabase.Substring(0, (PopulateTagDatabase.Length - 1));
                            PopulateTagDatabase += ";";
                            break;
                        default:
                            return;
                    }
                    //Console Comments
                    for (int i = 0; i < 100; i++) Console.Write("*");
                    Console.WriteLine("ENTERING " + TagItem.Parameter.ToUpper() + " FOR " + TagName.ToUpper());
                    Console.WriteLine("TIME ELAPSED " + sw.Elapsed);
                    Console.WriteLine("QUERIES MADE " + count);
                    for (int i = 0; i < 100; i++) Console.Write("*");

                    mySqlConn.Query(PopulateTagDatabase);
                }
            }
            //Exec Query
            //PopulateTagDatabase += ")";

        }

        private static void PopulateDaarchiveconfig(clsMySqlConnector MySqlConn, List<TagItem> TagItems)
        {
            //Clear previous data
            MySqlConn.Query("DELETE FROM Daarchiveconfig");

            //Create list of 1 robots and call it TagNames
            int NumberOfRobots = 1;
            ObservableCollection<string> TagNames = new ObservableCollection<string>();
            for (int i = 1; i < NumberOfRobots + 1; i++)
            {
                TagNames.Add("RB0" + i);
            }


            //IEnumerable<String> Parameter_Address = File.ReadLines("tags_address.txt");

            //INSERT RECORDS INTO DAARCHIVECONFIG
            //Loop through the {TagNames} which will be {DSConnName}
            //{TagName} are basically the robots name
            //{Parameter} are the sensor data type
            //{DSConnName} are the robots and will be {TagName}
            //{DataItemId} will be the values which I have to put from a text file
            //{TagId} will consist of {TagName}+"."+{Parameter}

            foreach (string TagName in TagNames)
            {
                //Loop through the {Paremeters}
                foreach (TagItem TagItem in TagItems)
                {
                    //Insert a Record
                    string QueryInsertATag = String.Format(@"insert into daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('{0}','{1}','{2}')", TagName, TagItem.Address, TagName + "." + TagItem.Parameter);
                    MySqlConn.Query(QueryInsertATag);
                }
            }
        }

    }



    public class TagValue
    {
        public TagValue()
        {

        }
        int Address { get; set; }
        string AddressDescriptoin { get; set; }
        DataType TagDataType { get; set; }
        string Range { get; set; }
    }


}


