﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace URDBQueries
{
    public enum DataType { Boolean, Angle, Velocity, Current, Temperature, Mode, RevolutionCount, State }
    class TagItem
    {
        public TagItem()
        {

        }
        public string Address { get; set; }
        public string Parameter { get; set; }
        public DataType TagDataType { get; set; }
        public string Range { get; set; }
    }
}
