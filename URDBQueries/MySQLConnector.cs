﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Net.Mail;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Data.Odbc;
using MySql.Data.MySqlClient;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.ComponentModel;
using OpenPlant;

namespace URDBQueries
{
    //---------------------------------------------------------------------------THIS CLASS IS FOR CONNECTING TO MySql
    public class clsMySqlConnector
    {
        public MySqlConnection MySqlConn;
        string _ConnectionString = "";
        public string ConnectionString
        {
            get
            {
                return _ConnectionString;
            }
            set
            {
                _ConnectionString = value;
                if (this.MySqlConn.State != ConnectionState.Open && this.MySqlConn.State != ConnectionState.Connecting) this.MySqlConn.ConnectionString = value;
            }
        }
        public string Name = "";

        [DebuggerStepThroughAttribute]
        public clsMySqlConnector()
        {
            this.MySqlConn = new MySqlConnection();
        }

        [DebuggerStepThroughAttribute]
        public clsMySqlConnector(string ConnectionString, string Name = "")
        {
            this.MySqlConn = new MySqlConnection(ConnectionString);
            this.ConnectionString = ConnectionString;
            this.Name = Name;
        }

        [DebuggerStepThroughAttribute]
        public void Setup(string ConnectionString)
        {
            this.MySqlConn = new MySqlConnection(ConnectionString);
            this.ConnectionString = ConnectionString;
        }


        [DebuggerStepThroughAttribute]
        public bool Connect()
        {
            if (this.MySqlConn.State != ConnectionState.Open)
            {
                string TID = System.Threading.Thread.CurrentThread.ManagedThreadId.ToString();
                try
                {
                    this.MySqlConn.Close();
                    this.MySqlConn.Open();
                }
                catch (Exception ex)
                {
                    // Logger.Log("[Thread " + TID + "] MySql Query - ERROR Unable to connect to MySql Server. Connection String = " + this.ConnectionString + "\r\n" + ex.ToString(),this.LogFilePath,true);
                    string dd = ex.ToString();
                    this.MySqlConn.Close();
                    return false;
                }
                //Logger.Log("[Thread " + TID + "] Successfully Opened MySQL Connection",this.LogFilePath);
            }
            return true;
        }

        [DebuggerStepThroughAttribute]
        public bool Connect(out string ExStr)
        {
            ExStr = "";
            if (this.MySqlConn.State != ConnectionState.Open)
            {
                string TID = System.Threading.Thread.CurrentThread.ManagedThreadId.ToString();
                try
                {
                    this.MySqlConn.Close();
                    this.MySqlConn.Open();
                }
                catch (Exception ex)
                {
                    // Logger.Log("[Thread " + TID + "] MySql Query - ERROR Unable to connect to MySql Server. Connection String = " + this.ConnectionString + "\r\n" + ex.ToString(),this.LogFilePath,true);
                    ExStr = ex.ToString();
                    this.MySqlConn.Close();
                    return false;
                }
                //Logger.Log("[Thread " + TID + "] Successfully Opened MySQL Connection",this.LogFilePath);
            }
            return true;
        }


        public readonly object lockerMySqlQuery = new Object();
        public bool TryQuery(string SQLQuery, out DataTable DTOut, out Exception ExOut)
        {
            DTOut = null;
            ExOut = null;
            try
            {
                DTOut = this.Query(SQLQuery);
            }
            catch (Exception ex)
            {
                ExOut = ex;
                return false;
            }
            return true;
        }

        [DebuggerStepThroughAttribute]
        public DataTable Query(string SQLQuery3)
        {

            string SQLQuery2 = SQLQuery3; //To allow cases when multiple calls are made and changes the value of SQLQuery
            lock (lockerMySqlQuery)
            {
                string SQLQuery = SQLQuery2;
                //0401133288
                Console.WriteLine(SQLQuery);
                //Logger.Log(SQLQuery,"sdfsd");
                DataTable _dt = new DataTable();
                var l = this.MySqlConn.State;
                using (var cmd = this.MySqlConn.CreateCommand())
                {
                    if (!this.Connect()) return null;
                    cmd.CommandText = SQLQuery;
                    //Logger.Log("MySql Query - Executing Query. Query : " + SQLQuery,"",this.LogFilePath);

                    using (MySqlDataReader Results = cmd.ExecuteReader())
                    {
                        // if (!Results.Read()) return null;

                        _dt.Columns.Clear();
                        //Logger.Log("MySql Query - Populating Data Table",this.LogFilePath);

                        for (int i = 0; i < Results.FieldCount; i++)
                        {
                            _dt.Columns.Add(Results.GetName(i));
                        }
                        //Logger.Log("MySql Query - Loading Results",this.LogFilePath);
                        _dt.Load(Results);
                    }

                }
                var d = this.MySqlConn.State;
                //Logger.Log("MySql Query - Complete",this.LogFilePath);
                return _dt;
            }
        }

        [DebuggerStepThroughAttribute]
        public DataTable Query(string SQLQuery, out string ExStr)
        {
            ExStr = "";
            lock (lockerMySqlQuery)
            {
                DataTable _dt = new DataTable();
                var l = this.MySqlConn.State;
                using (var cmd = this.MySqlConn.CreateCommand())
                {
                    if (!this.Connect(out ExStr))
                    {
                        return null;
                    }
                    cmd.CommandText = SQLQuery;
                    //Logger.Log("MySql Query - Executing Query. Query : " + SQLQuery,"",this.LogFilePath);

                    using (MySqlDataReader Results = cmd.ExecuteReader())
                    {
                        // if (!Results.Read()) return null;

                        _dt.Columns.Clear();
                        //Logger.Log("MySql Query - Populating Data Table",this.LogFilePath);

                        for (int i = 0; i < Results.FieldCount; i++)
                        {
                            _dt.Columns.Add(Results.GetName(i));
                        }
                        //Logger.Log("MySql Query - Loading Results",this.LogFilePath);
                        _dt.Load(Results);
                    }

                }
                var d = this.MySqlConn.State;
                //Logger.Log("MySql Query - Complete",this.LogFilePath);
                return _dt;
            }
        }

        [DebuggerStepThroughAttribute]
        public void Close()
        {
            if (this.MySqlConn.State == ConnectionState.Open)
            {
                try
                {
                    this.MySqlConn.Close();
                }
                catch
                {
                    //Logger.Log("WARNING: Unable to Close MySQL Connection. Current MySqlState is = " + this.MySqlConn.State, this.LogFilePath);
                }
            }
        }
    }
}