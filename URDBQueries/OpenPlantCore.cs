﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Data;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.Globalization;
using Microsoft.Win32;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Timers;
using System.ServiceModel.Channels;
using System.ComponentModel;
using System.Threading;

/*
    The following Assemblies need to be added
    -   Sytem.ServiceModel
*/

namespace OpenPlant
{

    #region GLOBAL *******************************************************************************************************************
    public static partial class Global
    {
        public static Dictionary<string, ProductDetails> Products = new Dictionary<string, ProductDetails>();
        public static string DesktopDir = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        public static string CommonAppDataDir = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
        public static bool AutomaticallyLoadConfigFile = true;

        //Licensing Stuff
        public static bool EnableLicense = false;
        public static bool EnableUUIDCheck = true;
        public static bool EnableLicenseExpiryCheck = true;

    }
    public enum Interpolation { None = 0, Linear = 1, Digital = 2 }
    public enum ViewRes { LoRes, HiRes }
    public enum DataType { Real, Int, Bool }

    #endregion


    #region PRODUCT DETAILS **********************************************************************************************************
    public partial class ProductDetails
    {
        public ProductDetails()
        {
        }        

        string _ProductName = "";
        public string ProductName
        {
            get
            {
                if (_ProductName == "")
                {
                    string AppName = System.AppDomain.CurrentDomain.FriendlyName;
                    string[] AppNameSplit = AppName.Split('.');
                    if (AppNameSplit.Count() > 0)
                    {
                        AppName = AppNameSplit[0];
                        //Check to see if Version is included in AppName
                        if (AppNameSplit.Count() > 1)
                        {
                            string Temp = AppNameSplit[0] + "." + AppNameSplit[1];
                            if (Regex.IsMatch(Temp, @"[0-9]+\.[0-9]+"))
                            {
                                AppName = Temp;
                                if (AppNameSplit.Count() > 2)
                                {
                                    string Temp2 = Temp + "." + AppNameSplit[2];
                                    if (Regex.IsMatch(Temp2, @"[0-9]+\.[0-9]+\.[0-9]+"))
                                    {
                                        AppName = Temp2;
                                    }
                                }
                            }
                        }
                    }
                    _ProductName = AppName;
                }
                return _ProductName;
            }
            set
            {
                _ProductName = value;
            }
        }

        string _ProductShortName ="";
        public string ProductShortName
        {
            get
            {
                if (_ProductShortName == "")
                {
                    throw new CustomException("Product Short name not set!");
                }
                return _ProductShortName;
            }
            set
            {
                _ProductShortName = value;
            }
        }


        public string ProductVersion
        {
            get
            {
                Assembly Assembly = Assembly.GetCallingAssembly();
                FileVersionInfo FileVersionInfo = FileVersionInfo.GetVersionInfo(Assembly.Location);
                string Version = FileVersionInfo.ProductVersion;
                return Version;
            }
        }

        public string SupportEmail = "Support@Open-Plant.com";

        string _ProgramDataDirectory = "";
        public string ProgramDataDirectory
        {
            get
            {
                if (_ProgramDataDirectory == "")
                {
                    string AppName = System.AppDomain.CurrentDomain.FriendlyName;
                    string[] AppNameSplit = AppName.Split('.');
                    if (AppNameSplit.Count() > 0) AppName = AppNameSplit[0];
                    _ProgramDataDirectory = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\" + AppName;
                    _ProductName = AppName;
                }
                return _ProgramDataDirectory;
            }
            set
            {
                if (value.EndsWith("\\")) value = value.Substring(0, value.Length - 1);
                if (value.StartsWith("\\")) value = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + value;
                if (value == Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData))
                {
                    throw new CustomException("FATAL ERROR: ProgramDataDirectory cannot be set to root directory");                    
                }
                Utilities.FindOrCreateDirectoryOrFail(value);
                _ProgramDataDirectory = value;
            }
        }

        Type _ConfigClassType =typeof(ConfigBase);
        public Type ConfigClassType //This is the object where the file is deserialized into
        {
            get
            {
                if (this.Config == null) return _ConfigClassType; else return this.Config.GetType();
            }
            set
            {
                _ConfigClassType = value;
            }
        } 

        string _ConfigFilePath = "";
        public string ConfigFilePath
        {
            get
            {
                if (_ConfigFilePath.StartsWith("\\")) _ConfigFilePath = this.ProgramDataDirectory + _ConfigFilePath;
                /*if (_Config==null && Global.AutomaticallyLoadConfigFile)
                {
                    this.TryLoadConfigFile(_ConfigFilePath);
                }*/
                return _ConfigFilePath;
            }
            set
            {
                string TempFP;
                _ConfigFilePath = value;
                if (_ConfigFilePath.StartsWith("\\")) TempFP = this.ProgramDataDirectory + _ConfigFilePath; else TempFP = _ConfigFilePath;
                iConfig TempConfig;
                if (!File.Exists(TempFP))
                {
                    if (this.ConfigClassType != null)
                    {
                        TempConfig = (iConfig)Activator.CreateInstance(this.ConfigClassType);
                        string BlankFPName = TempFP + ".Sample";
                        (new Serializer()).TrySerializeToXML<iConfig>(BlankFPName, TempConfig, this.LogFilePath);
                        throw new CustomException("FATAL ERROR: Config File Path not found at '" + TempFP + "'. A Blank Config File Path has been created ('" + BlankFPName + "'). You may modify this file and use it as a config file.");
                    }
                    else
                    {
                        throw new CustomException("FATAL ERROR: Config File Path not found at '" + TempFP + "'.");
                    }
                }
            }
        }


        public bool TryLoadConfigFile()
        {
            iConfig ConfigObject;
            iConfig DeserializedObject = null;
            bool Result = (new Serializer()).TryDeserializeFromXML(this.ConfigClassType, this.ConfigFilePath, out DeserializedObject);
            ConfigObject = DeserializedObject;
            ConfigObject.ProductDetails = this;
            this.Config = ConfigObject;
            return Result;
        }
        public bool TryLoadConfigFile(string ConfigFilePath)
        {
            iConfig ConfigObject;
            iConfig DeserializedObject = null;
            bool Result = (new Serializer()).TryDeserializeFromXML(this.ConfigClassType, ConfigFilePath, out DeserializedObject);
            ConfigObject = DeserializedObject;
            ConfigObject.ProductDetails = this;
            this.Config = ConfigObject;
            return Result;
        }

        string _LogFilePath ="";
        public string LogFilePath
        {
            get
            {
                if (_LogFilePath == "")
                {
                    _LogFilePath = this.Config.LogFilePath;
                }
                if (_LogFilePath == "") _LogFilePath = this.ProgramDataDirectory + "\\Logs\\" + ProductName + ".log";
                if (_LogFilePath.StartsWith("\\")) _LogFilePath = this.ProgramDataDirectory + _LogFilePath;
                return _LogFilePath;
            }
        }

        iConfig _Config = null;
        public iConfig Config { get { return _Config; }  set { _Config = value; } }


    }
    #endregion


    #region CONFIG *******************************************************************************************************************
    public partial interface iConfig
    {
        ProductDetails ProductDetails { get; set; } //XML IGNORED
        string ConfigFilePath { get; set; } //XML IGNORED
        bool ConfigInitialized { get; set; } //XML IGNORED
        bool SaveConfigFile(string ConfigFN); //XML IGNORED
        string LogFilePath { get; set; }       
        bool EnableLogToServer { get; set; }
        bool EnableLogToLocal { get; set; }
        string LogServerHost { get; set; }
        int LogServerPort { get; set; }
        int LogLocalPort { get; set; }
    }
    public partial class ConfigBase: iConfig
    {
        string _LogFilePath = @"\Logs\App.log";
        public string LogFilePath
        {
            get
            {
                if (_LogFilePath.StartsWith("\\")) return this.ProductDetails.ProgramDataDirectory + _LogFilePath;
                return _LogFilePath;
            }
            set { _LogFilePath = value; }
        }
        bool _EnableLogToServer = false; public bool EnableLogToServer { get { return _EnableLogToServer; } set { _EnableLogToServer = value; } }
        bool _EnableLogToLocal = false; public bool EnableLogToLocal { get { return _EnableLogToLocal; } set { _EnableLogToLocal = value; } }
        string _LogServerHost = "127.0.0.1"; public string LogServerHost { get { return _LogServerHost; } set { _LogServerHost = value; } }
        int _LogServerPort = 33171; public int LogServerPort { get { return _LogServerPort; } set { _LogServerPort = value; } }
        int _LogLocalPort = 33171; public int LogLocalPort { get { return _LogLocalPort; } set { _LogLocalPort = value; } }


        ProductDetails _ProductDetails = null;
        [XmlIgnore]
        public ProductDetails ProductDetails
        {
            get { if (_ProductDetails == null) throw new CustomException("Product Details not set!"); return _ProductDetails; }
            set { _ProductDetails = value; }
        }


        string _ConfigFilePath = "";
        [XmlIgnore]
        public string ConfigFilePath
        {
            get { return _ConfigFilePath; }
            set
            {
                if (value.StartsWith("\\")) _ConfigFilePath = ProductDetails.ProgramDataDirectory + value; else _ConfigFilePath = value;
            }
        }
        
        bool _ConfigInitialized = false;
        [XmlIgnore]
        public bool ConfigInitialized { get { return _ConfigInitialized; } set { _ConfigInitialized = value; } }



        public bool SaveConfigFile(string ConfigFilePath)
        {
            if (ConfigFilePath.StartsWith("\\"))
            {
                ConfigFilePath = ProductDetails.ProgramDataDirectory + ConfigFilePath;
            };
            return (new Serializer()).TrySerializeToXML(ConfigFilePath, this);
        }

        public bool SaveBlankConfigFile(string ConfigFilePath)
        {
            this.ProductDetails = new ProductDetails();
            return (new Serializer()).TrySerializeToXML(ConfigFilePath, this);
        }


        string ParseDir(string DirIn)
        {
            if (DirIn[0] == '\\') return this.ProductDetails.ProgramDataDirectory + DirIn;
            else return DirIn;
        }
    }
    #endregion


    #region UTILITIES ****************************************************************************************************************

    public partial class Utilities
    {
        public DateTime LastDateTimeNow = DateTime.Now;
        public ProductDetails ProductDetails;
        public Utilities(ProductDetails ProductDetails=null)
        {
            this.ProductDetails = ProductDetails;
        }

        public static T DeepClone<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }

        [DebuggerStepThroughAttribute]
        public T ObtainDataFromXMLFile<T>(string XMLFN, string XMLNode, T DefaultValue, bool LogResults = true, string Node = "")
        {
            string StrData = this.GetParameter(XMLFN, XMLNode);
            T Temp;
            if (StrData == "[]Error[]")
            {
                if (LogResults) if (this.ProductDetails!=null) Logger.Log(Node + "ERROR: unable to Obtain '" + XMLNode + "' from '" + XMLFN + "'. '" + XMLNode + "' Defaulted to " + DefaultValue.ToString(), this.ProductDetails.LogFilePath, true);
                Temp = DefaultValue;
            }
            else
            {
                try
                {
                    if (typeof(T).IsEnum)
                    {

                        Temp = (T)Enum.Parse(typeof(T), StrData, true);

                    }
                    else
                    {
                        Temp = (T)Convert.ChangeType(StrData, typeof(T), null);
                    }
                    if (this.ProductDetails != null) Logger.Log(Node + XMLNode + " set as '" + Temp + "'", this.ProductDetails.LogFilePath, false);
                }
                catch (Exception ex)
                {
                    if (LogResults) if (this.ProductDetails != null) Logger.Log(Node + "ERROR: unable to Obtain '" + XMLNode + "' from '" + XMLFN + "'. '" + XMLNode + "' Defaulted to " + DefaultValue.ToString() + "\r\n" + ex.ToString(), this.ProductDetails.LogFilePath, true);
                    Temp = DefaultValue;
                }
            }
            return Temp;
        }

        [DebuggerStepThroughAttribute]
        public DateTime UnixToDateTime(int epoch)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(epoch);
        }



        //Throttle the Thread
        Stopwatch stopwatch = new Stopwatch();
        bool FirstCallDone = false;
        long _actionStart;
        public void Throttle(int cpuPercentageLimit)
        {
            if (!FirstCallDone)
            {
                stopwatch.Reset();
                stopwatch.Start();
                _actionStart = stopwatch.ElapsedTicks;
                FirstCallDone = true;
            }
            else
            {
                long actionDuration = stopwatch.ElapsedTicks - _actionStart;
                long relativeWaitTime = (int)((1 / (double)cpuPercentageLimit) * actionDuration);
                int k = (int)((relativeWaitTime / (double)Stopwatch.Frequency) * 1000 * Environment.ProcessorCount);
                if (k > 10000) k = 10000;
                if (this.ProductDetails != null) Logger.Log("Throttler has instructed CPU to sleep for " + k + "ms", this.ProductDetails.LogFilePath, false);
                System.Threading.Thread.Sleep(k);
                stopwatch.Reset();
                stopwatch.Start();
                _actionStart = stopwatch.ElapsedTicks;
            }
        }

        private readonly object LockSaveParameter = new object();
        //[DebuggerStepThroughAttribute]
        public string GetParameter(string Path, string PName, bool ReturnEmptyStringIfError = false)
        {
            FileStream fs = null;
            lock (LockSaveParameter)
            {
                XDocument _XMLDoc = new XDocument();
                if (File.Exists(Path))
                {
                    string RetVal = "";
                    try
                    {
                        fs = File.OpenRead(Path);
                        _XMLDoc = XDocument.Load(fs);
                        XElement _Node = (from xml2 in _XMLDoc.Elements("Parameters").Elements(PName) select xml2).FirstOrDefault();
                        if (_Node == null)
                        {
                            if (ReturnEmptyStringIfError) RetVal = "";
                            RetVal = "[]Error[]";
                        }
                        else
                        {
                            RetVal = _Node.Value;//.ToString();
                        }
                        fs.Close();
                        fs = null;
                        return RetVal;
                    }
                    catch
                    {
                        if (ReturnEmptyStringIfError) return "";
                        return "[]Error[]";
                    }
                }
                else
                {
                    if (ReturnEmptyStringIfError) return "";
                    return "[]Error[]";
                }
            }
        }

        //This Subroutine saves a Parameter to an XML File	
        //[DebuggerStepThroughAttribute]
        public void SaveParameter(string Path, string PName, string PValue)
        {
            lock (LockSaveParameter)
            {
                XDocument _XMLDoc = new XDocument();
                FileStream fs = null;
                if (!File.Exists(Path))
                {
                    FileStream temp = File.Create(Path);
                    temp.Close();
                    File.AppendAllText(Path, "<Parameters></Parameters>");
                }
                try
                {
                    fs = File.OpenRead(Path);
                    _XMLDoc = XDocument.Load(fs);
                }
                catch
                {
                    FileStream temp = File.Create(Path);
                    temp.Close();
                    File.AppendAllText(Path, "<Parameters></Parameters>");
                    _XMLDoc = XDocument.Load(Path);
                }
                XElement _Node = _XMLDoc.Element("Parameters").Element(PName);
                //XElement _Node = (from xml2 in _XMLDoc.Elements("Parameters").Elements(PName) select xml2).FirstOrDefault();
                if (_Node == null)
                {
                    _XMLDoc.Element("Parameters").Add(new XElement(PName, PValue));
                }
                else
                {
                    _Node.Value = PValue;
                }
                try
                {
                    fs.Close();
                    using (StreamWriter sw = new StreamWriter(Path)) { sw.Write(_XMLDoc); }
                }
                catch (Exception ex)
                {
                    if (this.ProductDetails != null) Logger.Log("ERROR: Unabe to Save Parameter '" + PName + "' with Value '" + PValue + "' to File '" + Path + "'r\n" + ex.ToString(), this.ProductDetails.LogFilePath, true);
                }
            }
        }


        [DebuggerStepThroughAttribute]
        public string GetParameter(string FN, string PName)
        {
            lock (LockSaveParameter)
            {
                XmlDocument _XMLDoc = new XmlDocument();
                if (File.Exists(FN))
                {
                    try
                    {
                        _XMLDoc.Load(FN);
                        XmlNode _Node = _XMLDoc.SelectSingleNode("//" + PName);
                        if (_Node == null) return "[]Error[]"; else return _Node.InnerText;
                    }
                    catch
                    {
                        return "[]Error[]";
                    }
                }
                else return "[]Error[]";
            }
        }


        //-------------------------------------------------------------------------------------------------------------------------------------------------
        //This Subroutine find and creates a new directory
        [DebuggerStepThroughAttribute]
        public bool FindOrCreateDirectory(string DirIn)
        {
            if (!Directory.Exists(DirIn))
            {
                try
                {
                    Directory.CreateDirectory(DirIn);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            return true;
        }

        static readonly object FindOrCreateDirectoryOrFail_Lock = new object();
        public static void FindOrCreateDirectoryOrFail(string DirIn)
        {
            lock (FindOrCreateDirectoryOrFail_Lock)
            {
                if (!Directory.Exists(DirIn))
                {
                    try
                    {
                        Directory.CreateDirectory(DirIn);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("FATAL ERROR: Unable to create directory '" + DirIn + "'\r\n" + ex.ToString());
                    }
                }
            }
        }



        public double Get45or451FromRegistry()
        {
            using (RegistryKey ndpKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full\\"))
            {
                int releaseKey = Convert.ToInt32(ndpKey.GetValue("Release"));
                if (true)
                {
                    return CheckFor45DotVersion(releaseKey);
                }
            }
        }

        // Checking the version using >= will enable forward compatibility,  
        // however you should always compile your code on newer versions of 
        // the framework to ensure your app works the same. 
        private double CheckFor45DotVersion(int releaseKey)
        {
            if (releaseKey >= 393273)
            {
                return 4.6;
            }
            if ((releaseKey >= 379893))
            {
                return 4.52;
            }
            if ((releaseKey >= 378675))
            {
                return 4.51;
            }
            if ((releaseKey >= 378389))
            {
                return 4.5;
            }
            // This line should never execute. A non-null release key should mean 
            // that 4.5 or later is installed. 
            return 0;
        }

        [DebuggerStepThroughAttribute]
        public string FindFirstRegex(string input, string pattern)
        {
            try
            {
                System.Text.RegularExpressions.Regex _regex = new System.Text.RegularExpressions.Regex(@pattern);
                if (_regex.Match(input).Success) return _regex.Match(input).Groups[0].Value;
                else return "";
            }
            catch
            {
                return "";
            }
        }


        [DebuggerStepThroughAttribute]
        public string FindFirstRegexInBetween(string input, string patternwhole, string patterndeducted)
        {
            try
            {
                System.Text.RegularExpressions.Regex _regex = new System.Text.RegularExpressions.Regex(@patternwhole);
                string Whole = "", Deducted = "";
                if (_regex.Match(input).Success) Whole = _regex.Match(input).Groups[0].Value; else return "";
                _regex = new System.Text.RegularExpressions.Regex(@patterndeducted);
                if (_regex.Match(input).Success) Deducted = _regex.Match(input).Groups[0].Value; else return "";
                string Output = Whole.Substring(Deducted.Length, Whole.Length - Deducted.Length);
                return Output;
            }
            catch
            {
                return "";
            }
        }

        static readonly object LockSystemCheck = new Object();
        static bool _inSystemCheck = false;
        static TimeSpan start = System.Diagnostics.Process.GetCurrentProcess().TotalProcessorTime;
        static TimeSpan oldCPUTime = new TimeSpan(0);
        static DateTime lastMonitorTime = DateTime.UtcNow;
        public static void SystemCheck(ProductDetails ProductDetails=null)
        {
            lock (LockSystemCheck)
            {
                if (_inSystemCheck)
                {
                    return;
                }
                _inSystemCheck = true;
            }


            System.Diagnostics.Process CurProcess = System.Diagnostics.Process.GetCurrentProcess();
            string CurProcessID = CurProcess.Id.ToString();
            decimal memused = Math.Round(Convert.ToDecimal(CurProcess.WorkingSet64) / 1024 / 1024, 2);


            //Get Current Process CPU Usage
            TimeSpan newCPUTime = CurProcess.TotalProcessorTime - start;
            double DblCPUUsageLast = (newCPUTime - oldCPUTime).TotalSeconds / (Environment.ProcessorCount * DateTime.UtcNow.Subtract(lastMonitorTime).TotalSeconds);
            string CPUUsageLast = Math.Round(DblCPUUsageLast * 100, 2).ToString() + "%";
            lastMonitorTime = DateTime.UtcNow;
            oldCPUTime = newCPUTime;


            //Get CPU Usage
            System.Diagnostics.PerformanceCounter cpuCounter = new System.Diagnostics.PerformanceCounter();
            cpuCounter.CategoryName = "Processor";
            cpuCounter.CounterName = "% Processor Time";
            cpuCounter.InstanceName = "_Total";

            dynamic firstValue = cpuCounter.NextValue();
            System.Threading.Thread.Sleep(1000);
            dynamic secondValue = cpuCounter.NextValue();
            string CPUUsageTotal = Math.Round(secondValue, 2) + "%";

            if (ProductDetails != null) Logger.Log("********** SYSTEM USAGE CHECK >>> PID: " + CurProcessID + " (CPU: " + CPUUsageLast + ", MEMORY: " + memused + " MB ), Total Server CPU Usage: " + CPUUsageTotal + " **************", ProductDetails.LogFilePath);
            _inSystemCheck = false;
        }


        public static bool UnorderedEqual<T>(ICollection<T> a, ICollection<T> b)
        {
            // 1
            // Require that the counts are equal
            if (a.Count != b.Count)
            {
                return false;
            }
            // 2
            // Initialize new Dictionary of the type
            Dictionary<T, int> d = new Dictionary<T, int>();
            // 3
            // Add each key's frequency from collection A to the Dictionary
            foreach (T item in a)
            {
                int c;
                if (d.TryGetValue(item, out c))
                {
                    d[item] = c + 1;
                }
                else
                {
                    d.Add(item, 1);
                }
            }
            // 4
            // Add each key's frequency from collection B to the Dictionary
            // Return early if we detect a mismatch
            foreach (T item in b)
            {
                int c;
                if (d.TryGetValue(item, out c))
                {
                    if (c == 0)
                    {
                        return false;
                    }
                    else
                    {
                        d[item] = c - 1;
                    }
                }
                else
                {
                    // Not in dictionary
                    return false;
                }
            }
            // 5
            // Verify that all frequencies are zero
            foreach (int v in d.Values)
            {
                if (v != 0)
                {
                    return false;
                }
            }
            // 6
            // We know the collections are equal
            return true;
        }
    }
    
    [Serializable]
    public class CustomException : Exception
    {
        public CustomException()
            : base() { }

        public CustomException(string message)
            : base(message) { }

        public CustomException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public CustomException(string message, Exception innerException)
            : base(message, innerException) { }

        public CustomException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }
    }
    
    [DebuggerStepThroughAttribute]
    public class LegalFNCheck
    {
        public string cLegalFileName(string FN)
        {
            if (FN == "") return FN;
            string[] InvalidNames = new string[] { "CON", "PRN", "AUX", "CLOCK$", "NUL", "COM0", "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9", "LPT0", "LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9" };
            foreach (string s in InvalidNames)
            {
                if (FN == s) FN = "-{[" + s + "]}-";
                FN = FN.Replace(s + ".", "-{[" + s + "]}-.");
            }
            foreach (char c in System.IO.Path.GetInvalidFileNameChars()) //This contains the System.IO.Path.GetInvalidPathChars() Characters as well 
            {
                FN = FN.Replace(c.ToString(), "-{[" + (int)c + "]}-");
            }
            if (FN.Substring(0, 1) == ".")
            {
                FN = "-{[46]}-" + FN.Substring(1, FN.Length - 1);
            }
            if (FN.Right(1) == ".")
            {
                FN = FN.Substring(0, FN.Length - 1) + "-{[46]}-";
            }
            return FN;
        }


        //This subroutine converts a legal filename (modified by the above 'cLegalFileName' function) back to it's oiginal Name -------------------------------------------------------------------------------------------------
        public string cIllegalFileName(string FN)
        {
            string[] InvalidNames = new string[] { "CON", "PRN", "AUX", "CLOCK$", "NUL", "COM0", "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9", "LPT0", "LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9" };
            foreach (string s in InvalidNames)
            {
                FN = FN.Replace("-{[" + s + "]}-", s);
            }
            foreach (char c in System.IO.Path.GetInvalidFileNameChars())
            {
                FN = FN.Replace("-{[" + (int)c + "]}-", c.ToString());
            }
            FN = FN.Replace("-{[46]}-", ".");
            return FN;
        }
    }

    public class Wildcard : Regex
    {
        /// <summary>
        /// Initializes a wildcard with the given search pattern.
        /// </summary>
        /// <param name="pattern">The wildcard pattern to match.</param>
        public Wildcard(string pattern)
            : base(WildcardToRegex(pattern))
        {
        }

        /// <summary>
        /// Initializes a wildcard with the given search pattern and options.
        /// </summary>
        /// <param name="pattern">The wildcard pattern to match.</param>
        /// <param name="options">A combination of one or more
        /// <see cref="System.Text.RegexOptions"/>.</param>
        public Wildcard(string pattern, RegexOptions options)
            : base(WildcardToRegex(pattern), options)
        {
        }

        /// <summary>
        /// Converts a wildcard to a regex.
        /// </summary>
        /// <param name="pattern">The wildcard pattern to convert.</param>
        /// <returns>A regex equivalent of the given wildcard.</returns>
        public static string WildcardToRegex(string pattern)
        {
            return "^" + Regex.Escape(pattern).
             Replace("\\*", ".*").
             Replace("\\?", ".") + "$";
        }
    }

    #endregion

 

    #region EXTENSIONS ***************************************************************************************************************

    public static partial class Extensions
    {
        public static bool In<T>(this T obj, params T[] args)
        {
            return args.Contains(obj);
        }


        public static void RemoveAll(this IList list)
        {
            while (list.Count > 0)
            {
                list.RemoveAt(list.Count - 1);
            }
        }

        public static string UppercaseFirstLetter(this string value)
        {
            if (value.Length > 0)
            {
                char[] array = value.ToCharArray();
                array[0] = char.ToUpper(array[0]);
                return new string(array);
            }
            return value;
        }


        

        [DebuggerStepThroughAttribute]
        public static string Right(this string value, int length)
        {
            return value.Substring(value.Length - length);
        }

        [DebuggerStepThroughAttribute]
        public static SortedList<TKey, TValue> ToSortedList<TSource, TKey, TValue>
        (this IEnumerable<TSource> source,
         Func<TSource, TKey> keySelector,
         Func<TSource, TValue> valueSelector)
        {
            // TODO: Argument validation
            var ret = new SortedList<TKey, TValue>();
            foreach (var element in source)
            {
                ret.Add(keySelector(element), valueSelector(element));
            }
            return ret;
        }

        [DebuggerStepThroughAttribute]
        public static DateTime FromUnixTime(this long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }

        [DebuggerStepThroughAttribute]
        public static long ToUnixTime(this DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((date - epoch).TotalSeconds);
        }

        [DebuggerStepThroughAttribute]
        public static int ToInt32(this string value, string Node, out string ErrorLog)
        {
            int Temp = 0;
            ErrorLog = "";
            try
            {
                Temp = Convert.ToInt32(value);
            }
            catch (Exception ex)
            {
                ErrorLog = Node + "ERROR: Unable to Convert string '" + value + "' to Int32 DataType.\r\n" + ex.ToString();
            }
            return Temp;
        }

        [DebuggerStepThroughAttribute]
        public static Single ToSingle(this string value, string Node, out string ErrorLog)
        {
            Single Temp = 0;
            ErrorLog = "";
            try
            {
                Temp = Convert.ToSingle(value);
            }
            catch (Exception ex)
            {
                ErrorLog = Node + "ERROR: Unable to Convert string '" + value + "' to Single DataType.\r\n" + ex.ToString();
            }
            return Temp;
        }

        [DebuggerStepThroughAttribute]
        public static int ToInt(this object value)
        {
            return Convert.ToInt32(value);
        }

        [DebuggerStepThroughAttribute]
        public static int ToInt32(this object value, string Node, out string ErrorLog)
        {
            int Temp = 0;
            ErrorLog = "";
            try
            {
                Temp = Convert.ToInt32(value);
            }
            catch (Exception ex)
            {
                ErrorLog = Node + "ERROR: Unable to Convert object '" + value.ToString() + "' to int.\r\n" + ex.ToString();
            }
            return Temp;
        }

        [DebuggerStepThroughAttribute]
        public static Single ToSingle(this object value, string Node, out string ErrorLog)
        {
            Single Temp = 0;
            ErrorLog = "";
            try
            {
                Temp = Convert.ToSingle(value);
            }
            catch (Exception ex)
            {
                ErrorLog = Node + "ERROR: Unable to Convert object '" + value.ToString() + "' to Single DataType.\r\n" + ex.ToString();
            }
            return Temp;
        }



        [DebuggerStepThroughAttribute]
        public static double ToDouble(this object value, string Node = "")
        {
            double Temp = double.NaN;
            try
            {
                Temp = Convert.ToDouble(value);
            }
            catch
            {
            }
            return Temp;
        }




        [DebuggerStepThroughAttribute]
        public static double ToDouble(this object value, string Node, out string ErrorLog)
        {
            double Temp = double.NaN;
            ErrorLog = "";
            try
            {
                Temp = Convert.ToDouble(value);
            }
            catch (Exception ex)
            {
                ErrorLog = Node + "ERROR: Unable to Convert object '" + value.ToString() + "' to Double DataType.\r\n" + ex.ToString();
            }
            return Temp;
        }

        public static int WordCount(this string str)
        {
            string[] userString = str.Split(new char[] { ' ', '.', '?' },
                                        StringSplitOptions.RemoveEmptyEntries);
            int wordCount = userString.Length;
            return wordCount;
        }

        public static int LastDayOfTheMonth(this DateTime DT)
        {
            return DateTime.DaysInMonth(DT.Year, DT.Month);
        }

        public static DateTime FirstDayInMonth(this DateTime DT, DayOfWeek D)
        {
            DateTime Temp = new DateTime(DT.Year, DT.Month, 1, DT.Hour, DT.Minute, DT.Second);
            while (Temp.DayOfWeek != D) Temp = Temp.AddDays(1);
            return Temp;
        }

        public static DateTime LastDayInMonth(this DateTime DT, DayOfWeek D)
        {
            DateTime Temp = new DateTime(DT.Year, DT.Month, DateTime.DaysInMonth(DT.Year, DT.Month), DT.Hour, DT.Minute, DT.Second);
            while (Temp.DayOfWeek != D) Temp = Temp.AddDays(-1);
            return Temp;
        }

        public static int TotalCharWithoutSpace(this string str)
        {
            int totalCharWithoutSpace = 0;
            string[] userString = str.Split(' ');
            foreach (string stringValue in userString)
            {
                totalCharWithoutSpace += stringValue.Length;
            }
            return totalCharWithoutSpace;
        }

        static GregorianCalendar _gc = new GregorianCalendar();
        public static int GetWeekOfMonth(this DateTime time, CalendarWeekRule CWR, DayOfWeek DOW)
        {
            DateTime first = new DateTime(time.Year, time.Month, 1);
            return time.GetWeekOfYear(CWR, DOW) - first.GetWeekOfYear(CWR, DOW) + 1;
        }

        public static int GetWeekOfYear(this DateTime time, CalendarWeekRule CWR, DayOfWeek DOW)
        {
            return _gc.GetWeekOfYear(time, CWR, DOW);
        }

        public static DateTime LastDateTimeOfWeek(this DateTime time)
        {
            int Offset = 7 - (int)time.DayOfWeek;
            return time.AddDays(Offset);
        }


        public static DateTime FirstDateInWeek(this DateTime dt)
        {
            while (dt.DayOfWeek != System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.FirstDayOfWeek)
                dt = dt.AddDays(-1);
            return dt;
        }

        public static DateTime FirstDateInWeek(this DateTime dt, DayOfWeek weekStartDay)
        {
            while (dt.DayOfWeek != weekStartDay)
                dt = dt.AddDays(-1);
            return dt;
        }

        public static IEnumerable<T> DequeueChunk<T>(this Queue<T> queue, int chunkSize)
        {
            for (int i = 0; i < chunkSize && queue.Count > 0; i++)
            {
                yield return queue.Dequeue();
            }
        }
    }
    #endregion


    #region SERIALIZER ***************************************************************************************************************
    public class Serializer
    {
        readonly object SerializationLock = new Object();

        public bool TryDeserializeFromXML<T>(Type XMLClass, String FullPath, string LogFilePath, out T DeserializedObject)
        {
            try
            {
                lock (SerializationLock)
                {
                    XmlSerializer Deserializer = new XmlSerializer(XMLClass);
                    TextReader TextReader = new StreamReader(FullPath);
                    DeserializedObject = (T)Deserializer.Deserialize(TextReader);
                    TextReader.Close();
                    TextReader.Dispose();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Log("ERROR: Unable to Deserialize file '" + FullPath + "' which has type of '" + XMLClass.ToString() + "'\r\n" + ex.ToString(), LogFilePath, true);
                DeserializedObject = default(T);
                return false;
            }
            //}
        }

        public bool TryDeserializeFromXML<T>(Type XMLClass, String FullPath, out T DeserializedObject)
        {
            return TryDeserializeFromXML<T>(XMLClass, FullPath, "", out DeserializedObject);
        }

        static readonly object GlobalSerializationLock = new Object();

        //[DebuggerStepThroughAttribute]
        public bool TrySerializeToXML<T>(String FilePath, T t, string LogFilePath = "")
        {
            lock (SerializationLock)
            {
                XmlSerializer serializer = null;
                try
                {
                    serializer = new XmlSerializer(t.GetType());
                }
                catch (Exception ex)
                {
                    if (LogFilePath != "") Logger.Log("ERROR: Unable to Serialize object of type '" + typeof(T).ToString() + "'\r\n" + ex.ToString(), LogFilePath, true);
                    return false;
                }
                string FileDir = Path.GetDirectoryName(FilePath);
                if (!Directory.Exists(FileDir))
                {
                    try
                    {
                        Directory.CreateDirectory(FileDir);
                    }
                    catch
                    {
                        return false;
                    }
                }
                TextWriter textWriter = new StreamWriter(FilePath);
                serializer.Serialize(textWriter, t);
                textWriter.Close();
                textWriter.Dispose();
                return true;
            }
        }
    }
    #endregion
    

    #region WCF **********************************************************************************************************************
    public class WCFHost<clsType, iContract>
    {
        System.Timers.Timer WCFConnectionTimer = new System.Timers.Timer();
        ServiceHost ServiceHost = null;
        public WCFHost(int ListeningPort, int RefreshIntervalInMS = 15000)
        {
            this.ProductDetails = null;
            this.RefreshIntervalInMS = RefreshIntervalInMS;
            WCFConnectionTimer.Elapsed += new ElapsedEventHandler(WCFConnectionTimer_Elapsed); ;
            this.ListeningPort = ListeningPort;
        }
        public WCFHost(ProductDetails ProductDetails, int ListeningPort, int RefreshIntervalInMS = 15000)
        {
            this.ProductDetails = ProductDetails;
            this.RefreshIntervalInMS = RefreshIntervalInMS;
            WCFConnectionTimer.Elapsed += new ElapsedEventHandler(WCFConnectionTimer_Elapsed); ;
            this.ListeningPort = ListeningPort;
        }

        public ProductDetails ProductDetails { get; set; }
        public Host HostProtocol { get; set; }
        public int ListeningPort { get; set; }
        public int RefreshIntervalInMS = 15000;

        public enum Host { TCP = 1, HTTP = 2 };
        public void Start()
        {
            this.HostProtocol = HostProtocol;
            this.ListeningPort = ListeningPort;
            if (!_IsEnabled)
            {
                this.HostWCFTCP(); //Initialize for the first time
                WCFConnectionTimer_Elapsed(null, null);
                WCFConnectionTimer.Interval = Convert.ToDouble(this.RefreshIntervalInMS);
                WCFConnectionTimer.Enabled = true;
                if (this.ProductDetails != null) Logger.Log("WCF Connection Timer for '" + typeof(clsType) + "' Started. Timer Refreshes every " + WCFConnectionTimer.Interval + "ms", this.ProductDetails.LogFilePath);
            }
        }

        public void Stop()
        {
            this.ServiceHost.Close();
            _IsEnabled = false;
        }

        bool _IsEnabled = false; public bool IsEnabled { get { return _IsEnabled; } }
        bool _WCFinexecution = false;
        readonly object LockWCFConnectionTimer = new Object();
        private void WCFConnectionTimer_Elapsed(object sender, EventArgs e)
        {
            lock (LockWCFConnectionTimer)
            {
                if (_WCFinexecution) return;
                _WCFinexecution = true;
            }
            if (this.HostProtocol == Host.TCP) this.HostWCFTCP();
            _WCFinexecution = false;
        }


        //The connectWCF method is called every run time interval (which is set in the Config XML file, default value is 15 second)
        //This method does nothing if the Host Service has already started listening to a port
        public void HostWCFTCP()
        {
            //Check is WCF TCP host exists, if doesn't exist, create the object
            if (this.ServiceHost == null)
            {
                if (this.ProductDetails !=null) Logger.Log("Creating WCF TCP Service Host Object for '" + typeof(clsType) + "'", this.ProductDetails.LogFilePath);
                try
                {
                    this.ServiceHost = new ServiceHost(typeof(clsType));
                    if (this.ProductDetails != null) Logger.Log("Successfully Created WCF Service Host Object for '" + typeof(clsType) + "'", this.ProductDetails.LogFilePath);
                }
                catch (Exception ex)
                {
                    if (this.ProductDetails != null) Logger.Log("ERROR: Failed to Create WCF Service for '" + typeof(clsType) + "'\r\n" + ex.ToString(), this.ProductDetails.LogFilePath, true);
                }
            }
            if (this.ServiceHost != null)
            {
                if (this.ServiceHost.State != CommunicationState.Opened)
                {
                    //Open the communication port to start listening to the port                
                    try
                    {
                        if (this.ProductDetails != null) Logger.Log("Opening WCF Service Host for '" + typeof(clsType) + "', Listening on Port " + this.ListeningPort, this.ProductDetails.LogFilePath);
                        this.ServiceHost = new ServiceHost(typeof(clsType), new Uri("net.tcp://localhost:" + this.ListeningPort.ToString()));
                        ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                        smb.HttpGetEnabled = false;
                        this.ServiceHost.Description.Behaviors.Add(smb);

                        //Create Binding
                        //BinaryMessageEncodingBindingElement messageEncoding = new BinaryMessageEncodingBindingElement();
                        //TcpTransportBindingElement tcpTransport = new TcpTransportBindingElement();
                        //CustomBinding NTB = new CustomBinding(messageEncoding, tcpTransport);
                        NetTcpBinding NTB = new NetTcpBinding();
                        NTB.MaxReceivedMessageSize = 20000000; //Server.WCFMaxReceivedMessageSize;
                        Binding mexBinding = MetadataExchangeBindings.CreateMexTcpBinding();
                        
                        this.ServiceHost.AddServiceEndpoint(typeof(iContract), NTB, ""); //Add Endpoint to Host
                        this.ServiceHost.AddServiceEndpoint(typeof(iContract), mexBinding, "mex"); //Add Endpoint to Host
                        this.ServiceHost.Open();
                        if (this.ServiceHost.State == CommunicationState.Opened) Logger.Log("Successfully Opened WCF Service Host for '" + typeof(clsType) + "'", this.ProductDetails.LogFilePath);
                    }
                    catch (Exception ex)
                    {
                        if (this.ProductDetails != null) Logger.Log("ERROR: Unable to Open WCF Service Host for '" + typeof(clsType) + "'\r\n" + ex.ToString(), this.ProductDetails.LogFilePath, true);
                    }
                }
            }
        }
    }


    public class WCFClient<IContract>
    {

        public WCFClient(ProductDetails ProductDetails, string ServerHost, int ServerPort, int ConnectionRefreshIntervalInMS = 5000, int ConnectionRetryIntervalInMS = 10000)
        {
            ConnectionRefresh.Elapsed += new System.Timers.ElapsedEventHandler(ConnectionRefresh_Elapsed);
            this.ProductDetails = ProductDetails;
            this.ServerHost = ServerHost;
            this.ServerPort = ServerPort;
            this.ConnectionRetryIntervalInMS = ConnectionRetryIntervalInMS;
            this.ConnectionRefreshIntervalInMS = ConnectionRefreshIntervalInMS;
        }
        public WCFClient(string ServerHost, int ServerPort, int ConnectionRefreshIntervalInMS = 5000, int ConnectionRetryIntervalInMS = 10000)
        {
            ConnectionRefresh.Elapsed += new System.Timers.ElapsedEventHandler(ConnectionRefresh_Elapsed);
            this.ProductDetails = null;
            this.ServerHost = ServerHost;
            this.ServerPort = ServerPort;
            this.ConnectionRetryIntervalInMS = ConnectionRetryIntervalInMS;
            this.ConnectionRefreshIntervalInMS = ConnectionRefreshIntervalInMS;
            this.EnableActivityLog = false;
        }

        public ProductDetails ProductDetails { get; set; }
        bool _IsEnabled = false; public bool IsEnabled { get { return _IsEnabled; } }
        public string ServerHost { get; set; }
        public int ServerPort { get; set; }
        public ChannelFactory<IContract> WCFFactory { get; set; }
        public bool ConnectionOK = false;
        public IContract Channel = default(IContract);
        public int ConnectionRefreshIntervalInMS { get; set; }
        public int ConnectionRetryIntervalInMS { get; set; }
        System.Timers.Timer ConnectionRefresh = new System.Timers.Timer();
        public bool EnableActivityLog = true;


        public void Start()
        {
            if (!_IsEnabled)
            {
                _IsEnabled = true;
                ConnectionRefresh_Elapsed(null, null); //Established Initial Connection
                ConnectionRefresh.Interval = this.ConnectionRefreshIntervalInMS;
                ConnectionRefresh.Enabled = true;
            }
        }
        public void Stop()
        {
            _IsEnabled = false;
            this.ConnectionOK = false;
            ConnectionRefresh.Stop();
            ConnectionRefresh.Enabled = false;
        }


        //Refreshes the Connection
        readonly object ConnectToServerLock = new Object();
        bool _inConnectToServer = false;
        private void ConnectionRefresh_Elapsed(object sender, EventArgs e)
        {
            lock (ConnectToServerLock) if (_inConnectToServer) return; else _inConnectToServer = true;

            if (_IsEnabled && !this.ConnectionOK)
            {

                if (this.EnableActivityLog) if (this.ProductDetails != null) Logger.Log("Attempting to Connect to " + this.ServerHost + ":" + this.ServerPort + "'. Creating Channel Factory...", this.ProductDetails.LogFilePath);
                try
                {
                    NetTcpBinding NTB = new NetTcpBinding();
                    NTB.MaxReceivedMessageSize = 20000000;
                    WCFFactory = new ChannelFactory<IContract>(NTB, new EndpointAddress("net.tcp://" + this.ServerHost + ":" + this.ServerPort));
                    if (this.EnableActivityLog) if (this.ProductDetails != null) Logger.Log("Successfully Created WCF Channel Factory. Creating WCF Client...", this.ProductDetails.LogFilePath);
                    Channel = WCFFactory.CreateChannel();
                    if (this.EnableActivityLog) if (this.ProductDetails != null) Logger.Log("Connecting to TCP address '" + this.ServerHost + ":" + this.ServerPort + "'", this.ProductDetails.LogFilePath);
                    ((IClientChannel)Channel).Open();
                    if (this.EnableActivityLog) if (this.ProductDetails != null) Logger.Log("Successfully Connected to TCP address '" + this.ServerHost + ":" + this.ServerPort + "'", this.ProductDetails.LogFilePath);
                    this.ConnectionOK = true;
                }
                catch (Exception Ex)
                {
                    if (this.EnableActivityLog) if (this.ProductDetails != null) Logger.Log("ERROR: Unable to connect to Server. Retrying Connection after " + this.ConnectionRetryIntervalInMS + "ms (ConnectionRetryDelayInMS) \r\n" + Ex.ToString(), "", true);
                    this.ConnectionOK = false;
                    ConnectionRefresh.Stop();
                    System.Threading.Thread.Sleep(this.ConnectionRetryIntervalInMS);
                    ConnectionRefresh.Start();
                }
            }

            _inConnectToServer = false;
        }
    }

    #endregion WCF


    #region LOGGER *******************************************************************************************************************

    public struct LogStruct
    { 
        public string LogEntry { get; set; }
        public string LogFileEntry { get; set; }
        public string DestinationFileName { get; set; }
        public bool IsError { get; set; }
        public DateTime TimeStampInUTC { get; set; }
        public DateTime TimeStampInClientLocalTime { get; set; }
        public string AssemblyStr { get; set; }
        public string SourceCodeFileName { get; set; }
        public int SourceCodeLineNumber { get; set; }
    }

    public static class Logger
    {
        public static Queue<LogStruct> LogToServerQueue = new Queue<LogStruct>(Logger.LogQueueLimit);
        public static Queue<LogStruct> LogToLocalQueue = new Queue<LogStruct>(Logger.LogQueueLimit);
        static readonly object LogToServerQueueLock = new Object();
        static readonly object LogToLocalQueueLock = new Object();
        static WCFClient<ILSContract> WCFLogClient;
        static WCFClient<ILSContract> WCFLogClientLocal;
        public static int MaxLogFileSizeInMB = 40; //Default is 40MB
        public static int MaxNumberOfLogFiles = 100; //Default is 40MB
        public static int LogQueueLimit = 100000;
        public static Queue<LogStruct> LogQueue = new Queue<LogStruct>(Logger.LogQueueLimit);
        public static bool LoggerStarted = false;
        static ProductDetails _MainProductDetails = null;
        public static ProductDetails MainProductDetails
        {
            get
            {
                if (_MainProductDetails == null)
                {
                    if (Global.Products.Count == 0) throw new CustomException("FATAL ERROR: No Product Details have been set");
                    if (Global.Products.Count > 0)
                    {
                        Logger.MainProductDetails = Global.Products.Values.First();
                    }                    
                }
                return _MainProductDetails;
            }
            set
            {
                _MainProductDetails = value;
            }
        }
        static bool _Disabled = false;
        public static void DisableLogger()
        {
            _Disabled = true;
        }
        static readonly object LogQueueLock = new Object();
        static readonly object WriteLogLock = new Object();
        static readonly object LogLock = new Object();
        static int WritelogCounter = 500; //This is to allow file cheking only when the counter reaches > 250


        public static void ClearLog(string LogFullPath)
        {
            if (File.Exists(LogFullPath))
            {
                File.Delete(LogFullPath);
            }
        }
        public static void ClearLogs(string LogDir)
        {
            var LogDirInfo = new DirectoryInfo(LogDir);
            foreach (var File in LogDirInfo.EnumerateFiles("*.log"))
            {
                File.Delete();
            }
        }
        public static void EnableLogger()
        {
            LoggerStarted = true;
            _Disabled = false;
            new Thread(Logger.LoggerEngine).Start();

            if (Logger.MainProductDetails.Config.EnableLogToServer)
            {
                Logger.WCFLogClient = new WCFClient<ILSContract>(Logger.MainProductDetails.Config.LogServerHost, Logger.MainProductDetails.Config.LogServerPort);
                new Thread(Logger.LogToServer).Start();
            }

            if (Logger.MainProductDetails.Config.EnableLogToLocal)
            {
                if (Logger.MainProductDetails.Config.LogServerHost != "127.0.0.1" && Logger.MainProductDetails.Config.LogServerHost != "localhost")
                {
                    Logger.WCFLogClientLocal = new WCFClient<ILSContract>(Logger.MainProductDetails.Config.LogServerHost, Logger.MainProductDetails.Config.LogServerPort);
                    new Thread(Logger.LogToLocal).Start();
                }
            }
        }



        [DebuggerStepThroughAttribute]
        public static void WriteLog(String FullPath, String Log)
        {
            lock (WriteLogLock)
            {
                WritelogCounter = WritelogCounter + 1;
                try
                {
                    if (WritelogCounter >= 500)
                    {
                        if (File.Exists(FullPath))
                        {
                            FileInfo Fi = new FileInfo(FullPath);
                            if (Fi.Length > Logger.MaxLogFileSizeInMB * 1024 * 1024)
                            {
                                string OldLogFullPathWithoutExtension = Path.GetDirectoryName(FullPath) + "\\" + Path.GetFileNameWithoutExtension(FullPath) + "_" + DateTime.Now.ToString("yyyy-MM-dd_HHmm");
                                string OldLogFullPath = OldLogFullPathWithoutExtension + ".log";
                                int n = 1;
                                while (File.Exists(OldLogFullPath))
                                {
                                    OldLogFullPath = OldLogFullPathWithoutExtension + "(" + n + ").log";
                                    n += 1;
                                }
                                File.Move(FullPath, OldLogFullPath);

                                //Limit the number of files, Delete old files
                                DirectoryInfo LogDirectory = new DirectoryInfo(Path.GetDirectoryName(FullPath));
                                FileInfo[] LogFiles = LogDirectory.GetFiles(Path.GetFileNameWithoutExtension(FullPath) + "_*.log");
                                if (LogFiles.Count() > Logger.MaxNumberOfLogFiles)
                                {
                                    string OldestFile = "";
                                    for (int m = 0; m < LogFiles.Count(); m++)
                                    {
                                        if (string.Compare(OldestFile, LogFiles[m].Name) == 1) OldestFile = LogFiles[m].Name;
                                    }
                                    try
                                    {
                                        File.Delete(OldestFile);
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }

                            }
                        }
                        else
                        {
                            string DirPath = Path.GetDirectoryName(FullPath);
                            if (!Directory.Exists(DirPath))
                            {
                                try
                                {
                                    Directory.CreateDirectory(DirPath);
                                    Logger.Log("Successfully Created Log Directory '" + DirPath + "'", FullPath);
                                }
                                catch (Exception ex)
                                {
                                    Logger.Log("FATAL ERROR: Unable to create Log directory '" + DirPath + "'\r\n" + ex.ToString(), FullPath);
                                    throw new Exception("FATAL ERROR: Unable to create Log directory '" + DirPath + "'\r\n" + ex.ToString());
                                }
                            }
                        }
                        WritelogCounter = 0;
                    }
                    File.AppendAllText(@FullPath, Log);
                }
                catch (Exception ex)
                {
                    Logger.Log("ERROR: Unable to log." + ex.ToString(), FullPath, true);
                    WritelogCounter = 500;
                }
            }
        }

        //[DebuggerStepThroughAttribute]
        public static void Log(String LogEntry, String DestinationFileName, bool IsError = false)
        {
            if (_Disabled) return;
            if (!LoggerStarted)
            {
                Logger.EnableLogger(); // This also set LoggerStarted=true
            }
            lock (LogLock)
            {
                DateTime LogTimeStamp = DateTime.Now;


                StackFrame SF = (new System.Diagnostics.StackTrace(true)).GetFrame(1);
                string FP = SF.GetFileName();
                string SourceCodeFileName = System.IO.Path.GetFileName(FP);
                int SourceCodeLineNumber = -1;
                string Header;
                if (SourceCodeFileName != null)
                {
                    SourceCodeFileName = SourceCodeFileName.Substring(0, 10) + "~";
                    SourceCodeLineNumber = new System.Diagnostics.StackTrace(true).GetFrame(1).GetFileLineNumber();
                    Header = LogTimeStamp.ToString("dd/MM/yy HH:mm:ss.fff") + "  " + SourceCodeFileName + SourceCodeLineNumber;
                }
                else
                {
                    SourceCodeFileName = "";
                    Header = LogTimeStamp.ToString("dd/MM/yy HH:mm:ss.fff") + "  ";
                }
                string LogFileEntry = Header.PadRight(40) + LogEntry.Replace("\r\n", "\r\n".PadRight(54)) + "\r\n";
                if (DestinationFileName == "") throw new CustomException("Log destination not set");

                LogStruct LS =new LogStruct();
                LS.LogEntry = LogEntry;
                LS.LogFileEntry = LogFileEntry;
                LS.DestinationFileName = DestinationFileName;
                LS.IsError = IsError;
                LS.TimeStampInUTC = LogTimeStamp.ToUniversalTime();
                LS.TimeStampInClientLocalTime = LogTimeStamp;
                LS.AssemblyStr = Path.GetFileName(System.Reflection.Assembly.GetCallingAssembly().Location);
                LS.SourceCodeFileName = SourceCodeFileName;
                LS.SourceCodeLineNumber = SourceCodeLineNumber;
                lock (LogQueueLock) LogQueue.Enqueue(LS);
                if (Logger.MainProductDetails.Config.EnableLogToServer) lock (LogToServerQueueLock) LogToServerQueue.Enqueue(LS);
                if (Logger.MainProductDetails.Config.EnableLogToLocal) lock (LogToLocalQueueLock) LogToLocalQueue.Enqueue(LS);
            }
        }

        public static void LoggerEngine()
        {
            int LoqQueueCount;
            while (1 == 1)
            {
                lock (LogQueueLock) LoqQueueCount = LogQueue.Count;
                if (LoqQueueCount == 0)
                {
                    Thread.Sleep(100); //sleep 1ms if no log exists        
                    continue;
                }
                LogStruct LogObject;
                if (_Disabled) //If logger is disabled
                {
                    LoggerStarted = false;
                    return;
                }

                lock (LogQueueLock) LogObject = LogQueue.Dequeue();
                WriteLog(LogObject.DestinationFileName, LogObject.LogFileEntry);
                if (LogObject.IsError)
                {
                    string LogErrorFullPath = Path.GetDirectoryName(LogObject.DestinationFileName) + "\\" + Path.GetFileNameWithoutExtension(LogObject.DestinationFileName) + "-Error" + Path.GetExtension(LogObject.DestinationFileName);
                    WriteLog(LogErrorFullPath, LogObject.LogFileEntry); //This is to keep error all in one log for easy referencing    
                }
            }
        }

        public static void LogToServer()
        {
            int LoqQueueCount;
            while (1 == 1)
            {
                lock (LogToServerQueueLock) LoqQueueCount = LogToServerQueue.Count;
                if (LoqQueueCount == 0)
                {
                    Thread.Sleep(1000); //sleep 1s if no log exists        
                    continue;
                }
                List<LogStruct> LogObjects = new List<LogStruct>();
                if (_Disabled) //If Logger Disabled
                {
                    WCFLogClient.Stop();
                    return;
                }

                if (!WCFLogClient.IsEnabled) WCFLogClient.Start();

                for (int i = 0; i < LogToServerQueue.Count && LogToServerQueue.Count > 0; i++)
                {
                    LogStruct LS = LogToServerQueue.Dequeue();
                    LogObjects.Add(LS);
                }
                //LogObjects = LogToServerQueue.DequeueChunk(2000);
                if (WCFLogClient.ConnectionOK)
                {
                    bool SR = WCFLogClient.Channel.SendLogs(LogObjects);
                    DateTime DD = WCFLogClient.Channel.GetServerTimeUTC();
                }
                Thread.Sleep(1000);
            }
        }

        public static void LogToLocal()
        {
            int LoqQueueCount;
            while (1 == 1)
            {
                lock (LogToLocalQueueLock) LoqQueueCount = LogToLocalQueue.Count;
                if (LoqQueueCount == 0)
                {
                    Thread.Sleep(1000); //sleep 1s if no log exists        
                    continue;
                }
                IEnumerable<LogStruct> LogObjects;
                if (_Disabled) //If Logger Disabled
                {
                    WCFLogClientLocal.Stop();
                    return;
                }

                if (!WCFLogClientLocal.IsEnabled) WCFLogClientLocal.Start();
                LogObjects = LogToLocalQueue.DequeueChunk(2000);
                if (WCFLogClientLocal.ConnectionOK) WCFLogClientLocal.Channel.SendLogs(LogObjects);

                Thread.Sleep(1000);
            }
        }
    }
    #endregion
    #region LOGGER [WCF CONTACT] *****************************************************************************************************
    [ServiceContract]
    public interface ILSContract
    {
        [OperationContract]
        DateTime GetServerTimeUTC();

        [OperationContract]
        bool SendLogs(IEnumerable<LogStruct> LS);
    }

    #endregion
}
