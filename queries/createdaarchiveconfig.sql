DELETE FROM openplantserver.daarchiveconfig 
WHERE
    `DSConnName` = 'RB001';

insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','256','RB001.ControllerVersionHighNumber');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','257','RB001.ControllerVersionLowNumber');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','258','RB001.RobotMode');

insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','260','RB001.isPowerOnRobot');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','261','RB001.isProtectiveStopped');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','262','RB001.isEmergencyStopped');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','263','RB001.isFreedriveActive');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','264','RB001.isPowerPuttonPressed');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','265','RB001.isSafetySignalSuchThatWeShouldStop');

insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','270','RB001.BaseJointAngle');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','271','RB001.ShoulderJointAngle');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','272','RB001.ElbowJointAngle');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','273','RB001.Wrist1JointAngle');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','274','RB001.Wrist2JointAngle');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','275','RB001.Wrist3JointAngle');

insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','280','RB001.BaseJointAngleVelocity');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','281','RB001.ShoulderJointAngleVelocity');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','282','RB001.ElbowJointAngleVelocity');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','283','RB001.Wrist1JointAngleVelocity');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','284','RB001.Wrist2JointAngleVelocity');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','285','RB001.Wrist3JointAngleVelocity');

insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','290','RB001.BaseJointCurrent');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','291','RB001.ShoulderJointCurrent');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','292','RB001.ElbowJointCurrent');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','293','RB001.Wrist1JointCurrent');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','294','RB001.Wrist2JointCurrent');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','295','RB001.Wrist3JointCurrent');


insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','300','RB001.BaseJointTemperature');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','301','RB001.ShoulderJointTemperature');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','302','RB001.ElbowJointTemperature');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','303','RB001.Wrist1JointTemperature');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','304','RB001.Wrist2JointTemperature');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','305','RB001.Wrist3JointTemperature');


insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','310','RB001.BaseJointMode');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','311','RB001.ShoulderJointMode');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','312','RB001.ElbowJointMode');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','313','RB001.Wrist1JointMode');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','314','RB001.Wrist2JointMode');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','315','RB001.Wrist3JointMode');


insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','320','RB001.BaseJointRevolutionCount');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','321','RB001.ShoulderJointRevolutionCount');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','322','RB001.ElbowJointRevolutionCount');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','323','RB001.Wrist1JointRevolutionCount');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','324','RB001.Wrist2JointRevolutionCount');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','325','RB001.Wrist3JointRevolutionCount');

insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','450','RB001.RobotCurrent');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','451','RB001.IOCurrent');

insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','768','RB001.ToolState');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','769','RB001.ToolTemperature');
insert into openplantserver.daarchiveconfig(`dsconnname`,`daitemid`,`tagid`) values ('RB001','770','RB001.ToolCurrent');

SELECT 
    *
FROM
    openplantserver.daarchiveconfig;
    
SELECT 
    *
FROM
    openplantserver.tags;