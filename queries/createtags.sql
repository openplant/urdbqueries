insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','ControllerVersionHighNumber','0','[DA]RB001.ControllerVersionHighNumber');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','ControllerVersionLowNumber','0','RB001Database');

insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','RobotMode','0','RB001Database');

insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','isPowerOnRobot','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','isProtectiveStopped','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','isEmergencyStopped','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','isFreedriveActive','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','isPowerPuttonPressed','0','RB001Database');

insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','BaseJointAngle','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','ShoulderJointAngle','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','ElbowJointAngle','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','Wrist1JointAngle','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','Wrist2JointAngle','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','Wrist3JointAngle','0','RB001Database');

insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','BaseJointAngleVelocity','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','ShoulderJointAngleVelocity','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','ElbowJointAngleVelocity','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','Wrist1JointAngleVelocity','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','Wrist2JointAngleVelocity','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','Wrist3JointAngleVelocity','0','RB001Database');


insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','BaseJointCurrent','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','ShoulderJointCurrent','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','ElbowJointCurrent','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','Wrist1JointCurrent','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','Wrist2JointCurrent','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','Wrist3JointCurrent','0','RB001Database');

insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','BaseJointTemperature','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','ShoulderJointTemperature','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','ElbowJointTemperature','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','Wrist1JointTemperature','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','Wrist2JointTemperature','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','Wrist3JointTemperature','0','RB001Database');


insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','BaseJointMode','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','ShoulderJointMode','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','ElbowJointMode','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','Wrist1JointMode','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','Wrist2JointMode','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','Wrist3JointMode','0','RB001Database');

insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','BaseJointRevolutionCount','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','ShoulderJointRevolutionCount','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','ElbowJointRevolutionCount','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','Wrist1JointRevolutionCount','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','Wrist2JointRevolutionCount','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','Wrist3JointRevolutionCount','0','RB001Database');

insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','RobotCurrent','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','IOCurrent','0','RB001Database');

insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','ToolState','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','ToolTemperature','0','RB001Database');
insert into openplantserver.tags(`tagname`,`parameter`,`unitofmeasure`,`databaselocation`) values ('RB001','ToolCurrent','0','RB001Database');